(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(evil-collection evil org-drill nov hl-todo anzu smooth-scrolling git-gutter-fringe volatile-highlights indent-guide poly-R poly-markdown ess ace-window avy rainbow-delimiters ivy-rich which-key ivy-prescient counsel ivy modus-themes all-the-icons diminish use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
