(use-package magit
  :ensure t
  :diminish auto-revert-mode
  :bind (("C-x g" . magit-status)))

(provide 'init-git)
