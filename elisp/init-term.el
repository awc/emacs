(use-package vterm
  :commands vterm)

(use-package zmq)

(provide 'init-term)
